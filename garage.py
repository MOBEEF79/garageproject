""" Class definition for Garage """

from vehicle import Vehicle

class Garage():
    """ Class definition for Garage """

    _vehicles = []

    def __init__ (self):
        pass

    def list_all_vehicles(self):
        """ Function to list all vehicle objects in list """

        for vehicle in self._vehicles:
            attributes = vars(vehicle)
            print(type(vehicle),attributes)

    def remove_vehicle(self, index):
        """ Function to delete vehicle object from list """

        del self._vehicles[index]

    def add_vehicle(self, vehicle):
        """ Function to add vehicle object to list """

        self._vehicles.append(vehicle)

    def replace_vehicle(self, index_of_vehicle_to_replace, new_vehicle):
        """ Function to replace existing vehicle object in list with new vehicle """

        self._vehicles[index_of_vehicle_to_replace] = new_vehicle

    def quote_fix(self, index):
        """ Function to retrieve quote for fixing vehicle """

        attributes = vars(self._vehicles[index])
        print(f"The cost to fix {attributes} would be {self._vehicles[index].fix_vehicle()}")

    def repair_vehicle(self, index):
        """ Function to fix vehicle and remove it from the list """

        attributes = vars(self._vehicles[index])
        cost = self._vehicles[index].fix_vehicle()
        print(f"The cost to fix {attributes} was {cost:.2f}")
        self.remove_vehicle(index)

        return cost

    def repair_all_vehicles(self):
        """ Function to fix and remove all vehicle objects from list """

        total_cost = 0

        # Removing first item from list each time, so can just use index of zero repeatedly
        for counter in range(len(self._vehicles)):
            total_cost += self.repair_vehicle(0)
            counter += 1

        print(f"Total cost of fixing all vehicles was {total_cost:.2f}")

    def repair_all_vehicles_of_type(self, vehicle_type):
        """ Function fix and remove all vehicles of a chosen type from list """

        total_cost = 0
        items_removed = 0

        # Removing specific item from list each time
        # Need to keep count and reduce index to find next correct item in list
        for counter in range(len(self._vehicles)):
            if isinstance(self._vehicles[counter - items_removed], vehicle_type):
                total_cost += self.repair_vehicle(counter - items_removed)
                items_removed += 1
            counter += 1

        print(f"Total cost of fixing all vehicles of type {vehicle_type} was {total_cost:.2f}")

    def remove_all_vehicles_of_type(self, vehicle_type):
        """ Function to remove all vehicles of a chosen type from list """

        items_removed = 0

        # Removing specific item from list each time
        # Need to keep count and reduce index to find next correct item in list
        for counter in range(len(self._vehicles)):
            if isinstance(self._vehicles[counter - items_removed], vehicle_type):
                self.remove_vehicle(counter - items_removed)
                items_removed += 1
            counter += 1

        print(f"Removed all vehicles of type {vehicle_type}")
