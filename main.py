""" Script to test Garage class"""

#################################################################
#
# This script uses a file so you need to run it from the terminal
#
#################################################################

from garage import Garage
from vehicle import Car, Boat, Helicopter

my_garage = Garage()

with open("./cars.csv", "r", encoding = "utf-8") as File:
    tempFile = File.read().splitlines()
    for line in tempFile:
        data = (line.strip().split(","))
        new_car = Car(data[0], data[1], int(data[2]), data[3], eval(data[4]))
        my_garage.add_vehicle(new_car)

with open("./boats.csv", "r", encoding = "utf-8") as File:
    tempFile = File.read().splitlines()
    for line in tempFile:
        data = (line.strip().split(","))
        new_boat = Boat(data[0], data[1], eval(data[2]), eval(data[3]), int(data[4]))
        my_garage.add_vehicle(new_boat)

with open("./helicopters.csv", "r", encoding = "utf-8") as File:
    tempFile = File.read().splitlines()
    for line in tempFile:
        data = (line.strip().split(","))
        new_helicopter = Helicopter(data[0], data[1], int(data[2]), data[3], int(data[4]))
        my_garage.add_vehicle(new_helicopter)

print("\nGarage Functions")

# List all vehicles
print("\nList all vehicles:")
my_garage.list_all_vehicles()

# Remove "Sikorsky","Black",2,"Wheels",4
print('\nRemove vehicle "Sikorsky","Black",2,"Wheels",4')
my_garage.remove_vehicle(11)
my_garage.list_all_vehicles()

print('\nReplace vehicle "OldBoat","Rust",False,True,50 with new Boat:')
# Replace "OldBoat","Rust",False,True,50 with new Boat
replacement_boat = Boat("NewBoat","Orange",True,False,12)
my_garage.replace_vehicle(7, replacement_boat)
my_garage.list_all_vehicles()

# Quote to fix Porsche
print("\nQuote to fix Porsche:")
my_garage.quote_fix(1)

# Fix and remove Porsche
print("\nFix and remove Porsche:")
my_garage.repair_vehicle(1)
my_garage.list_all_vehicles()

# Fix all vehicles of type Car
print("\nFix and remove all vehicles of type Car:")
my_garage.repair_all_vehicles_of_type(Car)
print("Remaining vehicles in list:")
my_garage.list_all_vehicles()

# Remove all vehicles of type Boat
print("\nRemove all vehicles of type Boat:")
my_garage.remove_all_vehicles_of_type(Boat)
print("Remaining vehicles in list:")
my_garage.list_all_vehicles()

# Add cars back into list
with open("./cars.csv", "r", encoding = "utf-8") as File:
    tempFile = File.read().splitlines()
    for line in tempFile:
        data = (line.strip().split(","))
        new_car = Car(data[0], data[1], int(data[2]), data[3], eval(data[4]))
        my_garage.add_vehicle(new_car)

# Add boats back into list
with open("./boats.csv", "r", encoding = "utf-8") as File:
    tempFile = File.read().splitlines()
    for line in tempFile:
        data = (line.strip().split(","))
        new_boat = Boat(data[0], data[1], eval(data[2]), eval(data[3]), int(data[4]))
        my_garage.add_vehicle(new_boat)

# Fix all vehicles
print("\nFix and remove all vehicles:")
my_garage.repair_all_vehicles()
