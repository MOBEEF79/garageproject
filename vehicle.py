""" Class definitions for Vehicle, Car, Boat, Helicopter """

from abc import abstractmethod

class Vehicle():
    """ Class definition for Vehicle """

    def __init__ (self, brand, paint_type):
        self.brand = brand
        self.paint_type = paint_type

    @abstractmethod
    def fix_vehicle(self):
        """ Abstract implementation of fix_vehicle function """

class Car(Vehicle):
    """ Class definition for Car """

    def __init__ (self, brand, paint_type, doors, transmission, spoiler):
        super().__init__(brand, paint_type)
        self.doors = doors
        self.transmission = transmission
        self.spoiler = spoiler

    def fix_vehicle(self):
        """ Car implementation of fix_vehicle function """

        cost = 100

        if self.brand in ("BMW", "Porsche", "Ferrari", "Lambourghini", "Audi"):
            cost *= 2

        if self.paint_type == "Metallic":
            cost *= 1.2

        cost += self.doors * 10

        if self.transmission == "Automatic":
            cost += 100

        if self.spoiler:
            cost += 200

        return cost

class Boat(Vehicle):
    """ Class definition for Boat """

    def __init__ (self, brand, paint_type, has_engine, has_sails, length):
        super().__init__(brand, paint_type)
        self.has_engine = has_engine
        self.has_sails = has_sails
        self.length = length

    def fix_vehicle(self):
        """ Boat implementation of fix_vehicle function """

        cost = 100

        if self.brand in ("SeaShark", "JetTurbo", "HarbourMaster"):
            cost *= 2

        if self.paint_type == "Metallic":
            cost *= 1.2

        if self.has_engine:
            cost += 500

        if self.has_sails:
            cost += 100

        if self.length > 20:
            cost *= 1.5

        return cost

class Helicopter(Vehicle):
    """ Class definition for Helicopter """

    def __init__ (self, brand, paint_type, doors, wheels_or_skids, seats):
        super().__init__(brand, paint_type)
        self.doors = doors
        self.wheels_or_skids = wheels_or_skids
        self.seats = seats

    def fix_vehicle(self):
        """ Helicopter implementation of fix_vehicle function """

        cost = 100

        if self.brand in ("Sikorsky", "JetRanger", "Bell"):
            cost *= 2

        if self.paint_type == "Metallic":
            cost *= 1.2

        cost += self.doors * 10

        if self.wheels_or_skids == "Wheels":
            cost += 100

        if self.seats > 4:
            cost += (self.seats - 4) * 20

        return cost
